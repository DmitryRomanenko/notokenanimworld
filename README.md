# NoTokenAnim World
A fork of [Fyorl's NoTokenAnim](https://bitbucket.org/Fyorl/notokenanim) [Foundry VTT](http://foundryvtt.com/) module that provides an option to remove token movement animations.

The difference from original module is `scope: 'world'`.

Requires Foundry VTT version 0.4.4 or greater.
